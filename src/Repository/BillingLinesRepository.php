<?php

namespace App\Repository;

use App\Entity\BillingLines;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BillingLines|null find($id, $lockMode = null, $lockVersion = null)
 * @method BillingLines|null findOneBy(array $criteria, array $orderBy = null)
 * @method BillingLines[]    findAll()
 * @method BillingLines[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BillingLinesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BillingLines::class);
    }

    // /**
    //  * @return BillingLines[] Returns an array of BillingLines objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BillingLines
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
