<?php

namespace App\Entity;

use App\Repository\BillingLinesRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BillingLinesRepository::class)
 */
class BillingLines
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lineLabel;

    /**
     * @ORM\Column(type="integer")
     */
    private $lineNumber;

    /**
     * @ORM\Column(type="float")
     */
    private $linePrice;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLineLabel(): ?string
    {
        return $this->lineLabel;
    }

    public function setLineLabel(string $lineLabel): self
    {
        $this->lineLabel = $lineLabel;

        return $this;
    }

    public function getLineNumber(): ?int
    {
        return $this->lineNumber;
    }

    public function setLineNumber(int $lineNumber): self
    {
        $this->lineNumber = $lineNumber;

        return $this;
    }

    public function getLinePrice(): ?float
    {
        return $this->linePrice;
    }

    public function setLinePrice(float $linePrice): self
    {
        $this->linePrice = $linePrice;

        return $this;
    }
}
