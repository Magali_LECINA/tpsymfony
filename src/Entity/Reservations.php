<?php

namespace App\Entity;

use App\Repository\ReservationsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ReservationsRepository::class)
 */
class Reservations
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    
    /**
     * @ORM\Column(type="date")
     */
    private $startDate;
    
    /**
     * @ORM\Column(type="date")
     */
    private $endDate;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbAdults;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbChildren;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbAdultPool;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbChildrenPool;

    /**
     * @ORM\ManyToOne(targetEntity=Users::class, inversedBy="reservation")
     * @ORM\JoinColumn(nullable=false)
     */
    private $users;

    /**
     * @ORM\ManyToOne(targetEntity=Locations::class, inversedBy="reservation")
     * @ORM\JoinColumn(nullable=false)
     */
    private $locations;

    public function getId(): ?int
    {
        return $this->id;
    }
    
    public function getStartDate(): ?\DateTimeInterface
    {
        return $this->startDate;
    }
    
    public function setStartDate(\DateTimeInterface $startDate): self
    {
        $this->startDate = $startDate;
        
        return $this;
    }
    
    public function getEndDate(): ?\DateTimeInterface
    {
        return $this->endDate;
    }
    
    public function setEndDate(\DateTimeInterface $endDate): self
    {
        $this->endDate = $endDate;
    
        return $this;
    }

    public function getNbAdults(): ?int
    {
        return $this->nbAdults;
    }

    public function setNbAdults(int $nbAdults): self
    {
        $this->nbAdults = $nbAdults;

        return $this;
    }

    public function getNbChildren(): ?int
    {
        return $this->nbChildren;
    }

    public function setNbChildren(int $nbChildren): self
    {
        $this->nbChildren = $nbChildren;

        return $this;
    }

    public function getNbAdultPool(): ?int
    {
        return $this->nbAdultPool;
    }

    public function setNbAdultPool(int $nbAdultPool): self
    {
        $this->nbAdultPool = $nbAdultPool;

        return $this;
    }

    public function getNbChildrenPool(): ?int
    {
        return $this->nbChildrenPool;
    }

    public function setNbChildrenPool(int $nbChildrenPool): self
    {
        $this->nbChildrenPool = $nbChildrenPool;

        return $this;
    }

    public function getUsers(): ?Users
    {
        return $this->users;
    }

    public function setUsers(?Users $users): self
    {
        $this->users = $users;

        return $this;
    }

    public function getLocations(): ?Locations
    {
        return $this->locations;
    }

    public function setLocations(?Locations $locations): self
    {
        $this->locations = $locations;

        return $this;
    }
}
