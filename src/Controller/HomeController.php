<?php

namespace App\Controller;

use App\Entity\Users;
use App\Form\UserType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/index", name="index")
     */
    public function index(): Response
    {
        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }



    /**
     * @Route("/results", name="results")
     */
    public function results()
    {
        return $this->render('home/results.html.twig');
    }

    /**
     * @Route("/booking", name="booking")
     */
    public function booking(Request $request)
    {
        $user = $request->get('user');
        dump($request);
        return $this->render('home/booking.html.twig', [
            'user' => $user
        ]);
    }
    
    /**
     * @Route("/booking-view", name="booking-view")
     */
    public function bookingView()
    {
        return $this->render('home/booking-view.html.twig');
    }


    // ******* Formulaire de réservation ******* //

    /**
     * @Route("/usercreated", name="usercreated")
     */
    public function createUser(Request $request)
    {   
        $user = new Users();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        /* if($form->isSubmitted()&& $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            
            $em->flush($user);
            dump($user);
            return $this->redirectToRoute('booking',[
                'user' => $user
            ]); 
        }*/

        return $this->render('home/usercreated.html.twig',[
            'user' => $user,
            'form' => $form->createView()
        ]);

    }

    /**
     * @Route("/deluser/{id}", name="deluser")
     */
    public function deleteUser(Users $user, Request $request)
    {
    if($this->isCsrfTokenValid('delete'.$user->getId(), $request->get('_token'))) {
        $em = $this->getDoctrine()->getManager();
        $em->remove($user);
        $em->flush();
    return $this->redirectToRoute('booking');
    }

    }
}
