<?php

namespace App\DataFixtures;

use App\Entity\Prestations;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
    // Tableau de définition des prestations
        $prestations = [
            ['Mobil-home 3 personnes', '20'],
            ['Mobil-home 4 personnes', '24'],
            ['Mobil-home 5 personnes', '27'],
            ['Mobil-home 6 à 8 personnes', '34'],
            ['Caravane 2 places', '15'],
            ['Caravane 4 places', '18'],
            ['Caravane 6 places', '24'],
            ['Emplacement 8 m²','12'],
            ['Emplacement 12m²','14']
            ]; 

        foreach ($prestations as $key => $prestation){ // je parcours mon tableau
            $p = new Prestations(); // création de l'objet
            $p->setLabel($prestation[0]);
            $p->setPrice($prestation[1]);

            $this->setReference('P' . $key, $p ); // Participe à la création de la Foreign Key

            $manager->persist($p); // persistance de l'objet pour le flush en base
        }
        
        $manager->flush();
    }

}
