<?php

namespace App\DataFixtures;

use App\Entity\Locations;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker;

class LocationFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create('fr_FR');

        for($l = 0; $l < 10; $l++){
            $location = new Locations();
            $location->setDescription($faker->sentences(1, true))
                ->setPhoto("http://placehold.it/350x150")

                ->setPrestation($this->getReference('P'.rand(0, 8) ) );

            $manager->persist($location);
        }

        $manager->flush();
    }
}
