<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210708133824 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE billing (id INT AUTO_INCREMENT NOT NULL, users_id INT NOT NULL, identification_number INT NOT NULL, billing_date DATE NOT NULL, INDEX IDX_EC224CAA67B3B43D (users_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE billing_lines (id INT AUTO_INCREMENT NOT NULL, line_label VARCHAR(255) NOT NULL, line_number INT NOT NULL, line_price DOUBLE PRECISION NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE locations (id INT AUTO_INCREMENT NOT NULL, prestation_id INT NOT NULL, description VARCHAR(255) NOT NULL, photo VARCHAR(255) NOT NULL, INDEX IDX_17E64ABA9E45C554 (prestation_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE prestations (id INT AUTO_INCREMENT NOT NULL, label VARCHAR(255) NOT NULL, price DOUBLE PRECISION NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE reservations (id INT AUTO_INCREMENT NOT NULL, users_id INT NOT NULL, locations_id INT NOT NULL, start_date DATE NOT NULL, end_date DATE NOT NULL, nb_adults INT NOT NULL, nb_children INT NOT NULL, nb_adult_pool INT NOT NULL, nb_children_pool INT NOT NULL, INDEX IDX_4DA23967B3B43D (users_id), INDEX IDX_4DA239ED775E23 (locations_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE users (id INT AUTO_INCREMENT NOT NULL, firstname VARCHAR(255) NOT NULL, lastname VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, role VARCHAR(255) DEFAULT NULL, password VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE billing ADD CONSTRAINT FK_EC224CAA67B3B43D FOREIGN KEY (users_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE locations ADD CONSTRAINT FK_17E64ABA9E45C554 FOREIGN KEY (prestation_id) REFERENCES prestations (id)');
        $this->addSql('ALTER TABLE reservations ADD CONSTRAINT FK_4DA23967B3B43D FOREIGN KEY (users_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE reservations ADD CONSTRAINT FK_4DA239ED775E23 FOREIGN KEY (locations_id) REFERENCES locations (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE reservations DROP FOREIGN KEY FK_4DA239ED775E23');
        $this->addSql('ALTER TABLE locations DROP FOREIGN KEY FK_17E64ABA9E45C554');
        $this->addSql('ALTER TABLE billing DROP FOREIGN KEY FK_EC224CAA67B3B43D');
        $this->addSql('ALTER TABLE reservations DROP FOREIGN KEY FK_4DA23967B3B43D');
        $this->addSql('DROP TABLE billing');
        $this->addSql('DROP TABLE billing_lines');
        $this->addSql('DROP TABLE locations');
        $this->addSql('DROP TABLE prestations');
        $this->addSql('DROP TABLE reservations');
        $this->addSql('DROP TABLE users');
    }
}
